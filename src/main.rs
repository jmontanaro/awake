use std::env;
use std::fmt::Display;
use std::net::UdpSocket;

use pnet::datalink;


trait ResultExt<T> {
    fn unwrap_or_exit(self) -> T;
}

impl<T, E: Display> ResultExt<T> for Result<T, E> {
    fn unwrap_or_exit(self) -> T {
        match self {
            Ok(v) => v,
            Err(e) => {
                eprintln!("{}", e);
                std::process::exit(1);
            }
        }
    }
}


fn parse_mac(mac: &String) -> Result<[u8; 6], &'static str> {
    let mut bytes: [u8; 6] = [0; 6];
    let mut prev: Option<u8> = None;
    let mut i = 0;
    for c in mac.chars() {
        if let Some(n) = c.to_digit(16) {
            if i >= 6 {
                return Err("Invalid MAC address.");
            }
            match prev {
                Some(sixteens) => {
                    bytes[i] = sixteens * 16 + n as u8;
                    prev = None;
                    i += 1;
                },
                None => prev = Some(n as u8),
            }
        }
    }
    if i < 6 {
        return Err("Invalid MAC address.");
    }
    Ok(bytes)
}


fn construct_packet(addr: [u8; 6]) -> [u8; 102] {
    let mut packet: [u8; 102] = [255; 102];
    for i in 0..16 {
        for n in 0..6 {
            let x = 6 + i * 6 + n;
            packet[x] = addr[n];
        }
    }
    packet
}


fn send_udp(packet: [u8; 102], iface: &datalink::NetworkInterface) -> Result<usize, String> {
    let addr = match iface.ips.iter().next() {
        Some(ip) => ip.broadcast(),
        None => return Err(String::from("No IP addresses found for selected interface.")),
    };
    let destination = format!("{}:{}", addr.to_string(), "9");
    let socket = UdpSocket::bind("0.0.0.0:0").unwrap();
    socket.set_broadcast(true);
    println!("Sending wakeup packet to {}", &destination);
    socket.send_to(&packet, destination).map_err(|e| e.to_string())

}


fn find_interface(iface_name: &String) -> Result<datalink::NetworkInterface, &'static str> {
    let mut choice = Err("Interface not found.");
    for iface in datalink::interfaces() {
        if &iface.name == iface_name {
            choice = Ok(iface);
        }
    }
    choice
}


fn choose_interface() -> Result<datalink::NetworkInterface, &'static str> {
    let mut choice = Err("No interfaces available.");
    for iface in datalink::interfaces() {
        if iface.is_up() && !iface.is_loopback() {
            match choice {
                // if Ok, there are multiple interfaces we could use
                Ok(_i) => return Err("Please specify interface."),
                Err(_e) => choice = Ok(iface),
            }
        }
    }
    choice
}


struct Config {
    addr: [u8; 6],
}


fn parse_input(mut args: std::env::Args) -> Result<Config, &'static str> {
    args.next();
    match args.next() {
        Some(s) => parse_mac(&s).map(|addr| Config {addr}),
        None => Err("Please enter a destination address.")
    }
}


fn main() {
    let config = parse_input(env::args()).unwrap_or_exit();
    let packet = construct_packet(config.addr);
    let iface = choose_interface().unwrap_or_exit();
    send_udp(packet, &iface).unwrap_or_exit();
}
